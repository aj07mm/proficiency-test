class Course < ActiveRecord::Base
	has_many :classrooms

	validates :status, numericality: { only_integer: true }
	validates :name, :status, presence: true
end
