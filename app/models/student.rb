class Student < ActiveRecord::Base
	has_many :classrooms

	validates :status, :register_number , numericality: { only_integer: true }
end
