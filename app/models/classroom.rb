class Classroom < ActiveRecord::Base

  belongs_to :student
  belongs_to :course

  before_save :add_entry_at

  validates :student_id, :course_id, presence: true

  def add_entry_at
  	self.entry_at = Time.now
  end

end
