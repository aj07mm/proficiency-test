# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :classroom do
    entry_at "2015-01-01 15:56:01"
    student nil
    course nil
  end
end
